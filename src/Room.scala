
import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef

//exitKeys = Array[north, south, east, west] (all are strings)
class Room(val roomName: String, val roomKeyword: String, private var initialItems: List[Item], private val exitKeys: Array[String], description: String) extends ItemContainer {
  private var players: List[ActorRef] = Nil
  private var exits: Array[Option[ActorRef]] = null
  protected var items: List[Item] = initialItems

  def moveRooms(person: ActorRef, direction: String) = {
    getNextRoom(direction) match {
      case Some(room) => {
        person ! Main.UpdatePlayerRoom(room)
        person ! Main.Look
      }
      case None => person ! Main.Print("\tThere is not a room in that direction!")
    }
  }

  def getDescirption: String = description

  def getNextRoom(direction: String): Option[ActorRef] = {
    direction match {
      case "north" => exits(0)
      case "south" => exits(1)
      case "east" => exits(2)
      case "west" => exits(3)
      case _ => None
    }
  }

  def itemsInRoom: String = {
    var itemString = "In the room you see: \n"
    for (i <- items) {
      itemString += "\t" + i.name + ": " + i.description + "\n"
    }
    if (itemString == "In the room you see: \n") itemString += "\tNothing"
    itemString
  }
  
  def playersInRoom: String = {
    var playerString = "These people are in the room: \n"
    if(players.length == 0)
      playerString += "\tNobody."
    else
      for(p <- players)
        playerString += p.path.name + "\n"
    playerString
  }

  import Main._
  def receive = {
    case LinkRooms(rooms) => exits = exitKeys.map(s => if (s == "X") None else Some(rooms(s)))
    case LinkExitKeys => sender() ! ExitKey(roomKeyword, exitKeys)
    case PrintDefaultInfo => {
      val s = sender()
      s ! Print(getDescirption + "\n" + itemsInRoom + "\n" + playersInRoom)
    }
    case Look => sender() ! Print(getDescirption + "\n" + itemsInRoom + "\n" + playersInRoom)
    case Move(direction) => { val s = sender(); moveRooms(s, direction) }
    case GainItem(item) => addItem(item)
    case Speak(message) => {
      val s = sender()
      for(p <- players) 
        p ! Print(s.path.name + ": " + message)
      println(s.path.name + ": " + message)
    }
    case CallShortestPath(rm, endRoomName) => {
      rm ! FindShortestPath(roomKeyword, endRoomName, sender())
    }
    case AddSelfToRoom => {
      players ::= sender()
      println("The player " + sender.path.name + " is in the " + roomName + " room.")
      for(p <- players)
          p ! Main.Print(sender().path.name + " has entered the room.")
    }
    case RemoveSelfFromRoom => {
      players = players.filterNot(_ == sender())
    }
    case GrabItem(itemName) => {
      val s = sender()
      val (hasItemVal: Boolean, itemSpot: Int) = hasItem(itemName)
      if (hasItemVal) {
        s ! GainItem(dropItem(itemName, itemSpot))
        s ! Print("\tYou grab a " + itemName + " off the ground.")
      } else s ! Print("\tYou look around but there is no " + itemName + " to be found.")
    }
  }
}
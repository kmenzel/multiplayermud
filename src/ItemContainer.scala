
import akka.actor.Actor

abstract class ItemContainer extends Actor {
  protected var items:List[Item]
  def addItem(item: Item): Unit = items ::= item

  def hasItem(itemName: String): (Boolean, Int) = {
    for (i <- 0 to items.length - 1)
      if (items(i).name == itemName)
        return (true, i)
    (false, -1)
  }

  def dropItem(itemName: String, itemSpot: Int): Item = {
    val temp = new Item(items(itemSpot).name, items(itemSpot).description, items(itemSpot).equipable, items(itemSpot).damage, items(itemSpot).defense, items(itemSpot).timeDelay)
    items = items.filter(_.name != itemName)
    temp
  }
}
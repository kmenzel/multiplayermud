
class Item(val name: String, val description: String, val equipable: Boolean, val damage: Int, val defense: Int, val timeDelay:Int) {
  def isEquipable(): Boolean = equipable
  def getDamage(): Int = {
    if (equipable)
      damage
    else
      0
  }
  def getDefense(): Int = {
    if (equipable)
      defense
    else
      0
  }
}
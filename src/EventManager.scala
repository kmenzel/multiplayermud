
import akka.actor.Actor
import akka.actor.ActorRef

class EventManager extends Actor {
  import EventManager._
  private var time = 0
  private var queue = new SortedLLPriorityQueue[Event](eventComparator)
  private var queueSize = 0
  def receive = {
    case Update => {
      if(time % 50 == 0){
        println("Time:" + time)
        println("Queue size:" + queueSize)
        println(queue.peek())
      }
      time += 1
      if (queue.peek != None) {
        while (queue.peek != None && queue.peek.get.timeInterval <= time) {
          val event = queue.dequeue.get
          queueSize -= 1
          event.senderRef ! event.message
        }
      }
    }
    case Event(timeInterval, senderRef, message) => {
      println("EVENT SCHEDULED")
      queue.enqueue(new Event(timeInterval + time, senderRef, message))
      queueSize += 1
    }
  }
  
  //Returns true if the first element is a higherPriority than the second
  def eventComparator(e1: Event, e2:Event):Boolean = {
    if(e1.timeInterval <= e2.timeInterval)
      true
    else
      false
  }
}

object EventManager {
  case object Update
  case class Event(val timeInterval: Int, val senderRef: ActorRef, val message: Any)
}
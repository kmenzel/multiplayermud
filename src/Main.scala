import io.StdIn._
import xml._
import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintStream
import Main._
import akka.actor.Actor
import java.net.ServerSocket
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.Scheduler
import scala.concurrent.duration._

/* Things to do over summer:
 * Make grabbing items be lowercase
 * Have players of the same name
 * -> Seperate actor names from player/room names
 * Add purchasing system
 * Make health kits work
 * Different NPCs different healths
 * Talk to NPCs
 */

class Main(backstory: String, system: ActorSystem, em: ActorRef, pm: ActorRef, rm: ActorRef) extends Actor {
  def receive = {
    case IntroduceToGame => {
      val person = sender()
      person ! Print("\n\t\tWelcome to Karl's dope text adventure game yo!\n")
      person ! Print("\t\tType 'help' to see the list of commands.\n\n")
      person ! Print(backstory + "\n")
      person ! Main.PrintDefaultInfo
    }
    case PlayerDied(player) => {
      pm ! TellAll("Player " + player.path.name + " has been killed!!")
      system.stop(player)
    }
    case ProcessCommand(selection) => {
      val person = sender()
      if (selection == "help") {
        person ! Print("help -- Display the help menu. \nquit -- Quit the game. \nnorth -- Move north. \nsouth -- Move south. \neast -- Move east. \nwest -- Move west. \nlook -- See what's in the room. \ninv -- See your current inventory. \nget <item name> -- Pick up the specified item. \ndrop <item name> -- Drop the specified item. \nequip <item name> -- Equip the specified item. \nunequip -- Unequip the current weapon you're holding. \nkill <player name> -- Kill a certian player. \nshortestPath <room name> -- Print the shortest path to the specified room.")
      } else if (selection == "quit" || selection == "exit") {
        person ! Print("Good bye!")
        person ! Quit
      } else if (selection == "north" || selection == "south" || selection == "east" || selection == "west") {
        person ! Move(selection)
      } else if (selection == "look") person ! Look
      else if (selection == "inv" || selection == "inventory") person ! Inventory
      else if (selection == "unequip") person ! Unequip
      else if (selection.length() >= 12 && selection.substring(0, 12) == "shortestPath") {
        val roomName = selection.substring(13).trim()
        person ! CallShortestPath(rm, roomName)
      } else if (selection.length() >= 5 && selection.substring(0, 5) == "equip") {
        val itemName = selection.substring(6).trim()
        person ! Equip(itemName)
      } else if (selection.length() >= 3 && selection.substring(0, 3) == "get") {
        val itemName = selection.substring(4).trim()
        person ! GrabItem(itemName)
      } else if (selection.length() >= 4 && selection.substring(0, 4) == "drop") {
        val itemName = selection.substring(5).trim()
        person ! DropItem(itemName)
      } else if (selection.length() >= 4 && selection.substring(0, 4) == "tell") {
        val parsedSelection = selection.split(" ", 3)
        val recipiant = parsedSelection(1)
        val message = parsedSelection(2)
        system.actorSelection(person.path.parent).tell(Tell(message, recipiant), person)
      } else if (selection.length() >= 4 && selection.substring(0, 4) == "kill") {
        val playerName = selection.substring(5).trim()
        person ! NotifyAttack(playerName, pm)
      } else if (selection.length() >= 3 && selection.substring(0, 3) == "say") {
        val message = selection.substring(4).trim()
        person ! Speak(message)
      } else person ! Print("You want to do what? Type 'help' for the help menu.")
    }
  }
}

object Main {
  case class FindShortestPath(currentRoom: String, roomName: String, player: ActorRef)
  case class CallShortestPath(roomManager: ActorRef, roomName: String)
  case class Print(message: String)
  case class UpdatePlayerRoom(room: ActorRef)
  case class Move(direction: String)
  case class GainItem(item: Item)
  case class DropItem(itemName: String)
  case class GrabItem(itemName: String)
  case class LinkRooms(rooms: BSTMap[String, ActorRef])
  case class SetStartingRoom(person: ActorRef, roomName: String)
  case class AssignStartingRoom(room: ActorRef)
  case class SetUpRooms(n: NodeSeq)
  case class ProcessCommand(command: String)
  case class Speak(message: String)
  case class Tell(message: String, recipiant: String)
  case class TellAll(message: String)
  case class Swing(timeDelay: Int, damage: Int, attacker: ActorRef)
  case class Equip(itemName: String)
  case class PlayerDied(player: ActorRef)
  case class Attack(player: String, weapon: Item)
  case class NotifyAttack(player: String, playerManager: ActorRef)
  case class ExitKey(roomName: String, rooms: Array[String])
  case object Unequip
  case object LinkExitKeys
  case object Look
  case object Inventory
  case object PrintDefaultInfo
  case object IntroduceToGame
  case object CheckForInput
  case object Quit
  case object AddSelfToRoom
  case object RemoveSelfFromRoom

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("GameSystem")
    val rm = system.actorOf(Props[RoomManager], "RoomManager")
    val em = system.actorOf(Props[EventManager], "EventManager")
    val pm = system.actorOf(Props(new PlayerManager(rm, em)), "PlayerManager")
    val fileName = "BorderlandsMap.xml"
    val fileData = parseFile(fileName, rm)
    val main = system.actorOf(Props(new Main(fileData._1, system, em, pm, rm)), "Main")
    system.scheduler.schedule(0 seconds, 0.1 seconds, em, EventManager.Update)
    val ss = new ServerSocket(4292)
    val f1 = Future {
      while (true) {
        val sock = ss.accept()
        println("Connection established at " + sock + ".")
        val is = new BufferedReader(new InputStreamReader(sock.getInputStream()))
        val os = new PrintStream(sock.getOutputStream())
        val f2 = Future {
          os.println("What is your name?")
          pm ! PlayerManager.AddPlayer(is.readLine(), is, os, fileData._2, main, sock)
        }
      }
    }
  }

  def parseFile(fileName: String, rm: ActorRef): (String, String) = {
    val xmlFile = XML.loadFile(fileName)
    val backstory = (xmlFile \ "backstory").text.trim()
    val startRoom = (xmlFile \ "startingroom").text.trim()
    rm ! SetUpRooms(xmlFile \\ "room")
    (backstory, startRoom)
  }
}
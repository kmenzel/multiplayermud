import scala.reflect.ClassTag

class HeapPriorityQueue[A : ClassTag](higherP: (A, A) => Boolean) {
  var arr = new Array[A](10)
  var back = 1
  
  def enqueue(newElem:A) = {
    //Grow the array if it's not big enough
    if(back >= arr.length){
      val tmp = new Array[A](arr.length*2)
      for(i <- 0 until back) tmp(i) = arr(i)
      arr = tmp
    }
    
    //Determine the proper position to add the element
    var i = back
    while(i > 1 && higherP(newElem, arr(i/2))){
      arr(i) = arr(i/2)
      i /= 2
    }
    
    //Add the element
    arr(i) = newElem
    
    back += 1
  }
  
  def dequeue():A = {
    //The highest priority value... which has to be returned
    val ret = arr(1)
    //Remove the last value's spot
    back -= 1
    
    
    val temp = arr(back)
    var stone = 1
    var keepLooping = true
    //Keep looping as long as there are children && the temp value is higher priority than the highest priority child
    while(stone*2 < back && keepLooping){
      //Determine the higher priority child
      var higherPChild = stone*2
      if((stone*2)+1 < back && higherP(arr((stone*2)+1), arr(stone*2)))
        higherPChild += 1
      //Determine the second condition for our loop to keep looping
      if(higherP(temp, arr(higherPChild)))
        keepLooping = false
      else{
        //Move the higher priority child up one level and have the stone sink one level
        arr(stone) = arr(higherPChild)
        stone = higherPChild
      }
    }
    arr(stone) = temp
    
    ret
  }
  
  def peek():A = {
    arr(1)
  }
  
  def isEmpty() = back == 1
}
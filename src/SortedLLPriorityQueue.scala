
import EventManager._

//For higherP, true means the first element is of a higher priority than the second
class SortedLLPriorityQueue[A](higherP: (A, A) => Boolean) {
  case class Node(val data: A, var next: Node)
  var head: Node = null
  def enqueue(newElem : A) = {
    if (head == null || higherP(newElem, head.data)) {
      head = new Node(newElem, head)
    } else {
      var rover = head
      while (rover.next != null && higherP(rover.data, newElem)) {
        rover = rover.next
      }
      rover.next = new Node(newElem, rover.next)
    }
  }

  def dequeue(): Option[A] = {
    if(head == null)
      None
    else{
      val ret = head.data
      head = head.next
      Some(ret)
    }
  }

  def peek(): Option[A] = {
    if (head == null)
      None
    else
      Some(head.data)
  }

  def isEmpty(): Boolean = {
    (head == null)
  }
  
  def size(): Int = {
    var currentNode = head
    var ret = 0
    while(currentNode != null){
      ret += 1
      currentNode = currentNode.next
    }
    ret
  }
}
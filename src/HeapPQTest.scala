
object HeapPQTest extends App {
  val pq = new HeapPriorityQueue[Int](_ <= _)
  pq.enqueue(5)
  pq.enqueue(2)
  pq.enqueue(7)
  pq.enqueue(15)
  pq.enqueue(1)
  pq.enqueue(4)
  //println(pq.peek())
  println(pq.dequeue())
  println(pq.dequeue())
  println(pq.dequeue())
    println(pq.dequeue())
  println(pq.dequeue())
  println(pq.dequeue())
}

import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import java.io.PrintStream
import java.io.BufferedReader
import java.net.Socket

class NPC(val name: String, var currentRoom: ActorRef, val pm:ActorRef, val delay:Int) extends ItemContainer {
  private val directions = Map(0 -> "north", 1 -> "south", 2 -> "east", 3 -> "west")
  private val randomGen = scala.util.Random
  private var health:Int = 20
  private var currentWeapon:Item = null
  protected var items:List[Item] = Nil

  def changeRoom(newRoom: ActorRef):Unit = currentRoom = newRoom
  
  def takeAttack(damage:Int, attacker:ActorRef):Unit = {
    if (currentWeapon == null || (currentWeapon.getDefense() * 5) * math.random <= 100 - (currentWeapon.getDefense() * 5)) {
      health -= damage
      if (health <= 0)
        die(damage, attacker)
      else{
        attacker ! Main.Print(name + " is now at " + health + " after you did " + damage)
      }
    } else {
        attacker ! Main.Print(name + " blocked your attack!")
    }
  }
  
  def die(damage: Int, attacker:ActorRef): Unit = {
    attacker ! Main.Print("You killed " + name + ".")
    context.system.stop(this.context.self)
  }

  import Main._
  def receive = {
    case AssignStartingRoom(room) => {changeRoom(room); currentRoom ! AddSelfToRoom}
    case UpdatePlayerRoom(room) => {
      currentRoom ! RemoveSelfFromRoom
      changeRoom(room)
      currentRoom ! AddSelfToRoom
    }
    case Move(direction) => {
      sender() ! EventManager.Event(delay, this.self, new Move(directions(randomGen.nextInt(4))))
      currentRoom ! Move(direction)
    }
    case Swing(time, damage, s) => {
      println("The guard is getting a swing message!!!")
      if(currentWeapon != null)
        sender() ! EventManager.Event(currentWeapon.timeDelay, s, new Swing(currentWeapon.timeDelay, currentWeapon.damage, this.context.self))
      takeAttack(damage, s)
      sender() ! EventManager.Event(time, this.context.self, new Swing(time, damage, s))
    }
    case Equip(itemName) => {
      items.find(_.name == itemName) match {
        case Some(item) => {
          if(item.isEquipable())
            currentWeapon = item
        }
        case None => {}
      }
    }
    case NotifyAttack(playerName, pm) => {
      pm ! Attack(playerName, currentWeapon)
    }
  }
}
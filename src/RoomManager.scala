
import akka.actor.Actor
import akka.actor.Props
import xml._
import akka.actor.ActorRef

class RoomManager extends Actor {
  var rooms: BSTMap[String, ActorRef] = new BSTMap(_.compareTo(_))
  var roomExits: BSTMap[String, Array[String]] = new BSTMap(_.compareTo(_))
  val directions = Map(0 -> "N", 1 -> "S", 2 -> "E", 3 -> "W")
  //each exit array is = Array[north, south, east, west] (all are strings)

  import Main._
  def receive = {
    case SetStartingRoom(player, roomName) => player ! AssignStartingRoom(rooms(roomName))
    case SetUpRooms(node) => {
      val roomSeq = node.map(parseRoom)
      for (a <- roomSeq)
        rooms += a
      context.children.foreach(room => { room ! LinkRooms(rooms); room ! LinkExitKeys })
    }
    case FindShortestPath(currentRoom, roomName, player) => {
      player ! Print(shortestPath(currentRoom, roomName))
    }
    case ExitKey(roomName, roomExitStrings) => {
      roomExits += roomName -> roomExitStrings
    }
  }

  def shortestPath(currentRoom: String, finalRoom: String): String = {
    //This try-catch checks if the final destination room is even a real room
    try
      roomExits(finalRoom)
    catch {
      case (e: NoSuchElementException) => return "Room " + finalRoom + " not found!"
    }

    if (currentRoom == finalRoom)
      return "You are already in that room!"
    else
      return shortestPath(currentRoom, finalRoom, "", Nil).filter(_ != ':')
      //The colon is filtered out because it is used as a marker to signify that a path is correct
  }

  def shortestPath(currentRoom: String, finalRoom: String, currentPath: String, roomsAlreadyChecked: List[String]): String = {
    //If the method has been called on a room that doesn't exist, or we have already checked this room, it will return an X.
    if (currentRoom == "X" || roomsAlreadyChecked.contains(currentRoom)) {
      return "X"
    }
    //BASE CASE: If this is the room we are looking for then return the current path.
    if (currentRoom == finalRoom) {
      return currentPath + ":"
    }

    val northPath = shortestPath(roomExits(currentRoom)(0), finalRoom, currentPath + "N", currentRoom :: roomsAlreadyChecked)
    val southPath = shortestPath(roomExits(currentRoom)(1), finalRoom, currentPath + "S", currentRoom :: roomsAlreadyChecked)
    val eastPath = shortestPath(roomExits(currentRoom)(2), finalRoom, currentPath + "E", currentRoom :: roomsAlreadyChecked)
    val westPath = shortestPath(roomExits(currentRoom)(3), finalRoom, currentPath + "W", currentRoom :: roomsAlreadyChecked)
    
    var shortestPathRet = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    if (northPath.contains(":") && northPath.length() < shortestPathRet.length()) {
      shortestPathRet = northPath
    } 
    if (southPath.contains(":") && southPath.length() < shortestPathRet.length()) {
      shortestPathRet = southPath
    } 
    if (eastPath.contains(":") && eastPath.length() < shortestPathRet.length()) {
      shortestPathRet = eastPath
    } 
    if (westPath.contains(":") && westPath.length() < shortestPathRet.length()) {
      shortestPathRet = westPath
    }

    return shortestPathRet
  }

  def parseRoom(n: Node): (String, ActorRef) = {
    val roomName = (n \ "@name").text
    val roomKeyword = (n \ "@keyword").text
    var items: List[Item] = Nil
    val itemNode = (n \\ "item").iterator
    while (itemNode.hasNext) {
      val currentItem = itemNode.next()
      val itemName = (currentItem \ "@name").text
      val itemDesc = (currentItem \ "@desc").text
      val isEquipable = (currentItem \ "@equipable").text
      if (isEquipable == "true") {
        val damage = (currentItem \ "@damage").text.toInt
        val defense = (currentItem \ "@defense").text.toInt
        val delay = (currentItem \ "@delay").text.toInt
        items ::= new Item(itemName, itemDesc, true, damage, defense, delay)
      } else
        items ::= new Item(itemName, itemDesc, false, 0, 0, 0)
    }
    
    val npcIter = (n \\ "npc").iterator
    while (npcIter.hasNext){
      val currentNPC = npcIter.next()
      val npcName = (currentNPC \ "@name").text
      val npcDelay = (currentNPC \ "@delay").text.toInt
      context.system.actorSelection("akka://GameSystem/user/PlayerManager") ! PlayerManager.AddNPC(npcName, roomName, npcDelay)
      println("The npc " + npcName + " has been created!")
    }
    val exitNorth = ((n \ "exits") \ "@north").text
    val exitSouth = ((n \ "exits") \ "@south").text
    val exitEast = ((n \ "exits") \ "@east").text
    val exitWest = ((n \ "exits") \ "@west").text
    val exits = Array(exitNorth, exitSouth, exitEast, exitWest)

    val desc = (n \\ "desc").text.trim()
    val props = Props(new Room(roomName, roomKeyword, items, exits, desc))
    (roomKeyword, context.actorOf(props, roomKeyword))
  }

  private def comp(a: String, b: String): Int = if (a > b) 1 else if (b > a) -1 else 0
}

import akka.actor.Actor
import akka.actor.Props
import akka.actor.ActorRef
import java.io.PrintStream
import java.io.BufferedReader
import java.net.Socket

class Player(val name: String, var currentRoom: ActorRef, val is: BufferedReader, val os: PrintStream, val main: ActorRef, val sock: Socket) extends ItemContainer {
  protected var items: List[Item] = Nil
  private var health: Int = 20
  private var currentWeapon: Item = null

  def changeRoom(newRoom: ActorRef): Unit = currentRoom = newRoom

  def inv: String = {
    var inv = ""
    for (i <- items) {
      inv += i.name + ": " + i.description + "\n"
    }
    if (inv == "") inv = "You don't seem to be carrying any items!"
    inv
  }

  def takeAttack(damage: Int, attacker:ActorRef): Unit = {
    if (currentWeapon == null || (currentWeapon.getDefense() * 5) * math.random <= 100 - (currentWeapon.getDefense() * 5)) {
      health -= damage
      if (health <= 0)
        die(damage, attacker)
      else{
        os.println("Your health is now " + health + " after " + attacker.path.name + " dealt " + damage + " damage!")
        attacker ! Main.Print(name + " is now at " + health + " after you did " + damage)
      }
    } else {
        os.println("You successfully blocked " + attacker.path.name +"'s attack!")
        attacker ! Main.Print(name + " blocked your attack!")
    }
    
  }

  def die(damage: Int, attacker:ActorRef): Unit = {
    os.println("You took " + damage + " damage and are now dead!!!")
    attacker ! Main.Print("You killed " + name + ".")
    sock.close()
    context.system.stop(this.context.self)
  }

  import Main._
  def receive = {
    case IntroduceToGame => main ! IntroduceToGame
    case Print(message) => os.println(message)
    case Look => currentRoom ! Main.Look
    case Inventory => os.println(inv)
    case AssignStartingRoom(room) => { changeRoom(room); currentRoom ! AddSelfToRoom }
    case PrintDefaultInfo => currentRoom ! PrintDefaultInfo
    case UpdatePlayerRoom(room) => {
      currentRoom ! RemoveSelfFromRoom
      changeRoom(room)
      currentRoom ! AddSelfToRoom
    }
    case Move(direction) => currentRoom ! Move(direction)
    case GainItem(item) => addItem(item)
    case GrabItem(itemName) => currentRoom ! GrabItem(itemName)
    case Speak(message) => currentRoom ! Speak(message)
    case Quit => { println("Player " + name + " quit."); sock.close() }
    case DropItem(itemName) => {
      val (hasItemVal: Boolean, itemSpot: Int) = hasItem(itemName)
      if (hasItemVal) {
        if(currentWeapon != null && currentWeapon.name == itemName)
          currentWeapon = null
        currentRoom ! GainItem(dropItem(itemName, itemSpot))
        os.println("\tYou furiously throw a " + itemName + " to the floor.")
      } else os.println("\tYou look through your pockets but can't find a " + itemName + ".")
    }
    case Swing(time, damage, s) => {
      if(currentWeapon != null)
        sender() ! EventManager.Event(currentWeapon.timeDelay, s, new Swing(currentWeapon.timeDelay, currentWeapon.damage, this.context.self))
      takeAttack(damage, s)
      sender() ! EventManager.Event(time, this.context.self, new Swing(time, damage, s))
    }
    case CheckForInput => {
      if (is.ready()) {
        val input = is.readLine()
        main ! ProcessCommand(input)
      }
    }
    case Equip(itemName) => {
      items.find(_.name == itemName) match {
        case Some(item) => {
          if (item.isEquipable()) {
            currentWeapon = item
            os.println("\tYou equip a " + item.name + ".")
          } else os.println("\tA " + item.name + " is not equipable!")
        }
        case None => { os.println("\tYou do not have a " + itemName + "!") }
      }
    }
    case Unequip => {
      if(currentWeapon == null)
        os.println("\tYou are not using an item to unequip!")
      else{
        currentWeapon = null
        os.println("\tYou are no longer holding an item!")
      }
    }
    case CallShortestPath(rm, roomName) => {
      currentRoom ! CallShortestPath(rm, roomName)
    }
    case NotifyAttack(playerName, pm) => {
      pm ! Attack(playerName, currentWeapon)
    }
  }
}
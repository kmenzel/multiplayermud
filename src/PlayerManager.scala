
import akka.actor.Actor
import collection.mutable
import akka.actor.ActorRef
import java.io.BufferedReader
import java.io.PrintStream
import PlayerManager._
import akka.actor.Props
import java.io.InputStreamReader
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.net.Socket

class PlayerManager(val rm: ActorRef, val em: ActorRef) extends Actor {
  private val directions = Map(0 -> "north", 1 -> "south", 2 -> "east", 3 -> "west")
  private val randomGen = scala.util.Random
  val players = mutable.ArrayBuffer[ActorRef]()
  val f1 = Future {
    while (true) {
      context.children.foreach(_ ! Main.CheckForInput)
      Thread.sleep(1000)
    }
  }

  import Main._
  def receive = {
    case AddPlayer(name, is, os, startRoom, main, sock) => {
      val props = Props(new Player(name, null, is, os, main, sock))
      val person = context.actorOf(props, name)
      players += person
      rm ! SetStartingRoom(person, startRoom)
      Thread.sleep(1000)
      person ! IntroduceToGame
      println("A new player " + name + " has been added.")
      context.children.foreach(_ ! Print(name + " has joined the game!"))
    }
    case AddNPC(name, room, delay) => {
      println("The npc " + name + " has been added to the players!")
      val props = Props(new NPC(name, null, self, delay))
      val npc = context.actorOf(props, name)
      players += npc
      rm ! SetStartingRoom(npc, room)
      
      em ! EventManager.Event(delay, npc, new Move(directions(randomGen.nextInt(4))))
      println("A move event has been scheduled for the " + name)
    }
    case Tell(message, recipiantName) => {
      val s = sender()
      context.child(recipiantName) match {
        case Some(recipiant) => recipiant ! Print(s.path.name + " whispers: " + message)
        case None => s ! Print("\tPlayer " + recipiantName + " not found.")
      }
    }
    case Attack(playerName, weapon) => {
      val s = sender()
      if (weapon == null)
        s ! Print("You are not holding a weapon!!")
      else {
        context.child(playerName) match {
          case Some(player) => {println(player.path); em ! EventManager.Event(weapon.timeDelay, player, new Swing(weapon.timeDelay, weapon.damage, s))}
          case None => s ! Print("\tPlayer " + playerName + " not found.")
        }
      }
    }
    case TellAll(message) => {
      context.children.foreach(A => self ! Tell(message, A.path.name))
    }
  }
}

object PlayerManager {
  case class AddPlayer(name: String, is: BufferedReader, os: PrintStream, startRoom: String, main: ActorRef, sock: Socket)
  case class AddNPC(name:String, room:String, delay:Int)
}

class ListStack[A] {
  private case class Node(data:A, next:Node)
  private var top:Node = null
  
  def push(o: A): Unit = top = Node(o, top)
  def pop(): A = {
    val tmp = top.data
    top = top.next
    tmp
  }
  def peek: A = top.data
  def isEmpty: Boolean = top == null
}
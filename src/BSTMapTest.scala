object BSTMapTest extends App {
  var temp = new BSTMap[Int, Array[String]](_.compareTo(_))
  temp += (2 -> Array("A", "B", "C"))
  temp.get(2).get.foreach(println)
  temp += (1 -> Array("a", "b", "c"))
  temp.get(1).get.foreach(println)
  temp += (3 -> Array("D", "E", "F"))
  temp.get(3).get.foreach(println)
  println("Pre remove iterator: ")
  temp.iterator.foreach(println)
  temp -= 1
  println(temp.get(1))
  println("The iterator starts now: ")
  temp.iterator.foreach(println)
  //temp.get(3).get.foreach(println)
}
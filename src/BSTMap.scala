import scala.collection.mutable

class BSTMap[K, V](comp: (K, K) => Int) extends mutable.Map[K, V] {
  private class Node[K, V](var key: K, var value: V, var left: Node[K, V], var right: Node[K, V])
  private var root: Node[K, V] = null
  def get(key: K): Option[V] = {
    if (root == null)
      None
    else {
      var rover = root
      var c = comp(key, rover.key)
      while (rover != null && c != 0) {
        //val prevRover = rover
        if (c < 0)
          rover = rover.left
        else
          rover = rover.right
        if(rover != null)
          c = comp(key, rover.key)
      }
      
      if (rover == null) 
        None 
      else 
        Some(rover.value)
    }
  }

  def iterator = new Iterator[(K, V)] {
    val stack = new ListStack[Node[K, V]]

    def runLeft(n: Node[K, V]): Unit = {
      var rover = n
      while (rover != null) {
        stack.push(rover)
        rover = rover.left
      }
    }

    def next: (K, V) = {
      val n = stack.pop()
      runLeft(n.right)
      (n.key, n.value)
    }

    def hasNext: Boolean = !stack.isEmpty

    runLeft(root)
  }

  def +=(kv: (K, V)): BSTMap.this.type = {
    def helper(r: Node[K, V]): Node[K, V] = {
      if (r == null) new Node[K, V](kv._1, kv._2, null, null)
      else {
        val c = comp(kv._1, r.key)
        if (c == 0) r.value = kv._2
        else if (c < 0) r.left = helper(r.left)
        else r.right = helper(r.right)
        r
      }
    }
    root = helper(root)
    this
  }

  def -=(key: K): BSTMap.this.type = {
    
    def helper(r: Node[K, V]): Node[K, V] = {
      if (r != null) null
      else {
        val c = comp(key, r.key)
        if (c == 0){
          if(r.left == null)
            r.left
          else if (r.right == null)
            r.right
          else{
           val (k, v, node) = removeMax(r.left) 
           r.left = node
           r.key = k
           r.value = v
           r
          }
        } else if (c < 0){
          r.left = helper(r.left)
          r
        }
        else{ 
          r.right = helper(r.right)
          r        
        }    
      }
    }
    
    def removeMax(n: Node[K, V]):(K,V,Node[K, V]) = {
      if(n.right == null){
        (n.key, n.value, n.left) 
      } else {
        val (k, v, node) = removeMax(n.right)
        n.right = node
        (k, v, n)
      }
    }
    
    root = helper(root)
    this
  }

  def inOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      inOrder(n.left, f)
      f(n.value)
      inOrder(n.right, f)
    }
  }

  def preOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      f(n.value)
      preOrder(n.left, f)
      preOrder(n.right, f)
    }
  }

  def postOrder(n: Node[K, V], f: V => Unit): Unit = {
    if (n != null) {
      postOrder(n.left, f)
      postOrder(n.right, f)
      f(n.value)
    }
  }

}